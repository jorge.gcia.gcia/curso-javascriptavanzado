function *fibonaci  (){
    var index=1;
    var suma1=1;
    var suma2=1;
    yield 1;
    yield 1;
    while(index<100000)
    {
        let res = suma1+suma2;
        suma1=suma2;
        suma2=res;
        index++;
        yield suma2;
    }
}

var doIt = () =>
{
   var generador = fibonaci();
   for (var i = 0; i < 20 ; i++)
     console.log("Value: "+ generador.next().value);
}