var ok = false;

var promise = new Promise(function (resolve,reject){
    if ( ok == true)
       resolve("todo OK");
    else
       reject("error");
})


promise.then( result => {
    console.log(result);
},err => {
    console.log(err);
});
console.log("............ Finalizado .............");

var jqLoad = $.ajax("../data/data1.json");


jqLoad.then( response =>{
                console.log(response);
                for (let element of response )
                {
                   console.log(element.nombre+" "+element.apellidos);
                }
            },
             xhrObj=>{
                console.log(xhrObj);
             });

function get(url) {
// Return a new promise.
return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req
.onload = function() {
    // This is called even on 404 etc
    // so check the status
    if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
    }
    else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
    }
    };// Handle network errors
    req.onerror = function() {
    reject(Error("Network Error"));
    };// Make the request
    req.send();
});
}
get('../data/data2.json').then (response => {
    console.log("XMLHttpRequest ok\n"+response);
    let objects = JSON.parse(response);
    console.log(objects);
},
    error => {
        console.log("XMLHttpRequest error:"+error);
    });

// encadenamiento
get('../data/data1.json').then (response => {
    let objects = JSON.parse(response);
    return objects;
},
error => {
    console.log("XMLHttpRequest error:"+error);
})
.then (result => {
    console.log ("Encadenamiento:");
    for (let element of result )
    {
        console.log(element.nombre+" "+element.apellidos);
    }
});




var promise = new Promise ((resolve,reject) => {
    resolve(1);
});

promise
   .then(val => {
       console.log("Values: "+val);
       return val+2;
   })
   .then(val => {
       console.log("Values: "+val);
   })
   .catch(error => {
       console.log("Values error:"+error);
   });
              
// partes
var getJSON = UL => get(URL).then(JSON.parse);

