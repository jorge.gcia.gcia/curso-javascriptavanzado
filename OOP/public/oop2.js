var oopGo = function (){
    var log = function (message){
        console.log(message);
        var logElement = document.getElementById("results");
        logElement.innerHTML = logElement.innerHTML+message+"<br/>";
    }
    function Person(name)
    {
        this.name = name;
        this.Saludos = function (){
            log('Hola! Soy '+ this.name+".");       
        }
    }
    var jorge = new Person('Jorge');
    log(jorge.name);
    jorge.Saludos();
    jconsole.log(jorge.valueOf());
}
var anonimo = new Person('anonimo');