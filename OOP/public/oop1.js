var oopGo = function () {
    var log = function (message){
        console.log(message);
        var logElement = document.getElementById("results");
        logElement.innerHTML = logElement.innerHTML+message+"<br/>";
    }

    var persona = {
        nombre: ['Bob', 'Smith'],
        //nombre: {
        //  pila:'Bob',
        //  apellido:'Smith'
        //},
        edad: 32,
        genero: 'masculino',
        intereses: ['música', 'esquí'],
        bio: function () {
          log(this.nombre[0] + '' + this.nombre[1] + ' tiene ' + this.edad + ' años. Le gusta ' + this.intereses[0] + ' y ' + this.intereses[1] + '.');
        },
        saludo: function() {
          log('Hola, Soy '+ this.nombre[0] + '. ');
        }
      };
    log(persona.nombre);
    log(persona.nombre[0]);
    log(persona.nombre[1]);
    log(persona.genero);
    log(persona.intereses[1]);
    persona.bio();
    persona.saludo();
}