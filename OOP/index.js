const express = require("express");
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

//rutas estaticas
app.use(express.static(path.join(__dirname, 'public')));

// REST API
app.get('/hello',function (req,res) {
    res.send('version 1.0.0');
});
app.get('/people',function (req,res){
    res.send(
        [
            {
                id:1,
                firstname: 'paul',
                lastname:'fincher',
                age:59,
                sex:'male'
            },
            {
                id:2,
                firstname: 'paul',
                lastname:'fincher',
                age:29,
                sex:'male'
            },
            {
                id:3,
                firstname: 'paul',
                lastname:'fincher',
                age:43,
                sex:'male',
            },
            {
                id:4,
                firstname: 'paul',
                lastname:'fincher',
                age:21,
                sex:'male',
            },
            {
                id:5,
                firstname: 'paul',
                lastname:'fincher',
                age:30,
                sex:'male',
            },
            ]
    );
});

app.listen(8080, () => {
 console.log("El servidor está inicializado en el puerto 8080");
});