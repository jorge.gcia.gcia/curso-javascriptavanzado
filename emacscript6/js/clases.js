"use strict";

class Person {
    constructor (nombre,apellidos,fechaNacimiento) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = new Date(fechaNacimiento);
    }
    getAge() {
        var edadDifMs = Date.now() - this.fechaNacimiento.getTime();
        var ageDate = new Date(edadDifMs);
        return Math.abs(ageDate.getUTCFullYear()-1970);
    }
    get age() {
        return this.getAge();
    }
    set firstName (value){
        this.nombre = value;
    }
    get firstName ()
     {
         return this.nombre;
     }
    static getClassName ()
    {
        return "Person class";
    }

}

class PersonExtender extends Person
{
    constructor(nombre,apellidos,fechaNacimiento){
        super(nombre,apellidos,fechaNacimiento);
    }
    toString()
    {
        return this.nombre+" | "+this.apellidos+" | " + this.fechaNacimiento;
    }
}

class Person2 {
    // atributos privados
    #nombre="";
    #apellidos="";
    #fechaNacimiento="";

    constructor(nombre,apellidos,fechaNacimiento) {
        this.#nombre = nombre;
        this.#apellidos = apellidos;
        this.#fechaNacimiento = fechaNacimiento;
    }
    set nombre (value ) {
        this.#nombre = value;
    }
    get nombre (){
        return this.#nombre;
    }
    set apellidos  (value ){
        this.#apellidos = value;
    }
    get apellidos (){
        return this.#apellidos;
    }
    set fechaNacimiento  (value ){
        this.#fechaNacimiento = value;
    }
    get fechaNacimiento (){
        return this.#fechaNacimiento;
    }
}

var doIt = () => {
    console.log(typeof Person);
    try {
        Person('pablo','montoya','01-30-1980');
    }
    catch (e)
    {
        console.log("No se puede invocar el constructor de un objeto sin new");
    }
    var p = new Person('pablo','montoya','01-30-1980');
    console.log(Person.getClassName());
    console.log(p);
    console.log(p.getAge());
    console.log(p.age);
    p.firstName = "jorge";
    console.log(p.firstName);
    console.log("==================================================================================");
    var p2 = new Person2("jorge","garcia","03-28-1972");
    console.log(p2.nombre+" "+p2.apellidos+" "+p2.fechaNacimiento);
    p2.Nombre = "JUAN";
    console.log(p2.nombre+" "+p2.apellidos+" "+p2.fechaNacimiento);
    console.log("==================================================================================");
    var p3 = new PersonExtender("jose","prieto","04-10-1992");
    console.log(p3.toString());
    
}