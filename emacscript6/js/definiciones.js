function configureLog()
{
   var previousConsole = console.log;
   console.log = function(...message) {
       previousConsole(...message);
       let element = document.createElement("p");
       element.appendChild(document.createTextNode(message[0]));
       document.getElementById('debugDiv').appendChild(element);};
   console.error = console.debug = console.info =  console.log
}

function doIt ()
{
    console.clear();
    configureLog();


    modifyConst ('Nuevo valor para constante');
    usingLet();
    arrow();
    defaultParams();
    restParams();
}

// DEFINICION DE CONSTANTES

function modifyConst (valor)
{
    console.log("");
    console.log("[constantes]");

    const URL = "www.sidertia.com";
    try{
        URL=valor;
    }catch(e)
    {
        console.log("ERROR:", e);
    }
}

// USO DE LET

function usingLet ()
{
    console.log("");
    console.log ("[utilizar let]");
    if (true)
    {
        var ambitoFuncion="Tengo ambito de funcion";
        let ambitoBloque="Tengo ambito de bloque";
    }
    try {
        console.log("AMBITO DE FUNCION: "+ ambitoFuncion);
        console.log("AMBITO DE BLOQUE: "+ambitoBloque);
    }catch(e)
    {
        console.log("ERROR:",e);
    }
}

// ARROW

function arrow ()
{
    console.log("[operador ")
    console.log("");
    var vocales = ['a','e','i','o','u'];
    vocales.forEach(value => {
        console.log ("vocal: "+ value);
    })

    var coordenadas = [[0,1],[2,3],[4,5],[6,7]];
    coordenadas.forEach ( x =>{
        console.log ("coordenada: ("+x+")");
    });

    var sumaDos = (a,b) => a+b;
    console.log ("sumaDos: "+sumaDos(12,23));

    var sumaTres = (a,b,c) => {
        var d = a + b;
        return d+c;
    }
    console.log ("sumaTres: "+sumaTres(1,2,3));

}

var defaultParams = () =>
{
    function imprimirContacto (nombre,apellidouno ="garcia",apellidodos="garcia")
    {
        console.log ("hola "+nombre+" "+apellidouno+" "+apellidodos);
    }
    imprimirContacto("jorge");
}

var restParams = () => 
{
    // ES5
    function  imprimirNombresES5 (name) {
        var length = arguments.length;
        var fullName = name;
        if (length > 1){
            for (var i=1; i < length; i++){
                fullName += ' ' + arguments[i];
            }
        }
        console.log(fullName);
    }
    imprimirNombresES5 ("ES5:","Jorge","Garcia","Garcia");

    // ES6
    var imprimirNombresES6 = (...message) => {
        var fullName = "";
        message.forEach(param => fullName+= ' '+param);
        console.log(fullName);
    }
    // para quitar el espacio del inicio:
    var imprimirNombresES6Mejorado = (nombre,...message) => {
        var fullName = nombre;
        message.forEach(param => fullName+= ' '+param);
        console.log(fullName);
    }
    imprimirNombresES6("ES6:","Antonio","Del Alba","Perez");
    imprimirNombresES6Mejorado("ES6Mejorado:","Antonio","Del Alba","Perez");
}