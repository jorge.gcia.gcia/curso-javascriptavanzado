Conceptos básicos de los objetos JavaScript
==

En el primer artículo sobre los objetos de JavaScript, veremos la sintaxis fundamental de los objetos de JavaScript y revisaremos algunas características de JavaScript que ya hemos analizado anteriormente en el curso, reiterando el hecho de que muchas de las funciones con las que ya ha tratado de hecho son objetos.
Prerrequisitos: 	Conocimientos básicos de informática, conocimientos básicos de HTML y CSS, familiaridad con los principios básicos de JavaScript (consulte Primeros pasos y Building blocks ).
Objetivo: 	Para comprender la teoría básica detrás de la programación orientada a objetos, cómo esto se relaciona con JavaScript ("la mayoría de las cosas son objetos") y cómo empezar a trabajar con objetos de JavaScript.

Un objeto es una colección de datos relacionados y / o funcionalidad (que generalmente consta de varias variables y funciones, que se denominan propiedades y métodos cuando están dentro de objetos). Vamos a trabajar a través de un ejemplo para mostrarnos cómo son.

Para empezar, haga una copia local de nuestro archivo oojs.html . Esto contiene muy poco: un elemento < script> para que escribamos nuestro código fuente en un elemento < input> para que ingresemos instrucciones de muestra en el momento en que se procesa la página , algunas definiciones de variables y una función que genera cualquier código ingresado en la entrada en un elemento <p>. Usaremos esto como una base para explorar la sintaxis básica de los objetos.

Al igual que con muchas cosas en JavaScript, la creación de un objeto a menudo comienza con la definición e inicialización de una variable. Intente ingresar lo siguiente debajo del código JavaScript que ya está en su archivo, luego guarde y actualice:

    var persona = {};

Si ingresa persona su entrada de texto y presiona el botón, debe obtener el siguiente resultado:

    [objeto Objeto]

Felicitaciones, acabas de crear tu primer objeto. ¡Trabajo hecho! Pero este es un objeto vacío, por lo que realmente no podemos hacer mucho con él. Actualicemos nuestro objeto para que se vea así:

    var persona = {
      nombre: ['Bob', 'Smith'],
      edad: 32,
      genero: 'masculino',
      intereses: ['música', 'esquí'],
      bio: function () {
        alert(this.nombre[0] + '' + this.nombre[1] + ' tiene ' + this.edad + ' años. Le gusta ' + this.intereses[0] + ' y ' + this.intereses[1] + '.');
      },
      saludo: function() {
        alert('Hola, Soy '+ this.nombre[0] + '. ');
      }
    };

Después de guardar y actualizar, intente ingresar algunos de los siguientes en su entrada de texto:

    persona.nombre
    persona.nombre[0]
    persona.edad
    persona.intereses[1]
    persona.bio()
    persona.saludo()

¡Ahora tiene algunos datos y funcionalidades dentro de su objeto, y ahora puede acceder a ellos con una sintaxis simple y agradable!

Nota: Nota: Si tiene problemas para hacer que esto funcione, intente comparar su código con nuestra versión - vea oojs-finished.html (también vea que se ejecuta en vivo). La versión en vivo le dará una pantalla en blanco, pero eso está bien. De nuevo, abra sus devtools e intente escribir los comandos anteriores para ver la estructura del objeto.

Entonces, ¿qué está pasando aquí? Bien, un objeto se compone de varios miembros, cada uno de los cuales tiene un nombre (por ejemplo, nombre y edad) y un valor (por ejemplo, ['Bob', 'Smith'] y 32). Cada par nombre/valor debe estar separado por una coma, y el nombre y el valor en cada caso están separados por dos puntos. La sintaxis siempre sigue este patrón:

    var nombreObjeto = {
      miembro1Nombre: miembro1Valor,
      miembro2Nombre: miembro2Valor,
      miembro3Nombre: miembro3Valor
    }

El valor de un miembro de un objeto puede ser prácticamente cualquier cosa: en nuestro objeto persona tenemos una cadena de texto, un número, dos matrices y dos funciones. Los primeros cuatro elementos son elementos de datos y se denominan propiedades del objeto. Los dos últimos elementos son funciones que le permiten al objeto hacer algo con esos datos, y se les denomina métodos del objeto.

Un objeto como este se conoce como un objeto literal — literalmente hemos escrito el contenido del objeto tal como lo fuimos creando. Esto está en contraste con los objetos instanciados de las clases, que veremos más adelante.

Es muy común crear un objeto utilizando un objeto literal cuando desea transferir una serie de elementos de datos relacionados y estructurados de alguna manera, por ejemplo, enviando una solicitud al servidor para ponerla en una base de datos. Enviar un solo objeto es mucho más eficiente que enviar varios elementos individualmente, y es más fácil de procesar que con una matriz, cuando desea identificar elementos individuales por nombre.
Notación de punto
Sección

Arriba, accediste a las propiedades y métodos del objeto usando notación de punto (dot notation). El nombre del objeto (persona) actúa como el espacio de nombre (namespace); al cual se debe ingresar primero para acceder a cualquier elemento encapsulado dentro del objeto. A continuación, escriba un punto y luego el elemento al que desea acceder: puede ser el nombre de una simple propiedad, un elemento de una propiedad de matriz o una llamada a uno de los métodos del objeto, por ejemplo:

    persona.edad
    persona.intereses[1]
    persona.bio()

Espacios de nombres secundarios

Incluso es posible hacer que el valor de un miembro del objeto sea otro objeto. Por ejemplo, intente cambiar el miembro nombre de

    nombre: ['Bob', 'Smith'],

a

    nombre : {
      pila: 'Bob',
      apellido: 'Smith'
    },

Aquí estamos creando efectivamente un espacio de nombre secundario (sub-namespace). Esto suena complejo, pero en realidad no es así: para acceder a estos elementos solo necesitas un paso adicional que es encadenar con otro punto al final. Prueba estos:

    persona.nombre.pila
    persona.nombre.apellido

Importante: en este punto, también deberá revisar su código y cambiar cualquier instancia de

    nombre[0]
    nombre[1]

a

    nombre.pila
    nombre.apellido

De lo contrario, sus métodos ya no funcionarán.
Notación de corchetes

Hay otra manera de acceder a las propiedades del objeto, usando la notación de corchetes. En lugar de usar estos:

    persona.edad
    persona.nombre.pila

Puedes usar

    persona['edad']
    persona['nombre']['pila']

Esto se ve muy similar a cómo se accede a los elementos en una matriz, y básicamente es lo mismo: en lugar de usar un número de índice para seleccionar un elemento, se esta utilizando el nombre asociado con el valor de cada miembro. No es de extrañar que los objetos a veces se denominen arreglos asociativos: mapean cadenas de texto a valores de la misma manera que las arreglos mapean números a valores.

Establecer miembros de objetos

Hasta ahora solo hemos buscado recuperar (u obtener) miembros del objeto: también puede establecer (actualizar) el valor de los miembros del objeto simplemente declarando el miembro que desea establecer (usando la notación de puntos o corchetes), de esta manera:

    persona.edad = 45;
    persona['nombre']['apellido'] = 'Cratchit';

Intenta ingresar estas líneas y luego vuelve a ver a los miembros para ver cómo han cambiado:

    persona.edad
    persona['nombre']['apellido']

Establecer miembros no solo es actualizar los valores de las propiedades y métodos existentes; también puedes crear miembros completamente nuevos. Prueba estos:

    persona['ojos'] = 'avellana';
    persona.despedida = function() { alert("¡Adiós a todos!"); }

Ahora puede probar a los nuevos miembros:

    persona['ojos']
    person.despedida()

Un aspecto útil de la notación de corchetes es que se puede usar para establecer dinámicamente no solo los valores de los miembros, sino también los nombres de los miembros. Digamos que queríamos que los usuarios puedan almacenar tipos de valores personalizados en sus datos personales, escribiendo el nombre y el valor del miembro en dos entradas de texto. Podríamos obtener esos valores de esta manera:

    var nombrePerzonalizado = entradaNombre.value;
    var valorPerzonalizado = entradaValor.value;

entonces podríamos agregar este nuevo miembro nombre y valor al objeto persona de esta manera:

persona[nombrePerzonalizado] = valorPerzonalizado;

Para probar esto, intente agregar las siguientes líneas en su código, justo debajo de la llave de cierre del objeto persona:

    var nombrePerzonalizado = 'altura';
    var valorPerzonalizado = '1.75m';
    persona[nombrePerzonalizado] = valorPerzonalizado;

Ahora intente guardar y actualizar, e ingrese lo siguiente en su entrada de texto:

    persona.altura

Agregar una propiedad a un objeto no es posible con la notación de puntos, que solo puede aceptar un nombre de miembro literal, no un valor variable que apunte a un nombre.
¿Qué es "this" (este)?

Es posible que hayas notado algo un poco extraño en nuestros métodos. Mira esto, por ejemplo:

    saludo: function() {
      alert('¡Hola!, Soy '+ this.nombre.pila + '.');
    }

Probablemente se esté preguntando qué es "this". La palabra clave this se refiere al objeto actual en el que se está escribiendo el código, por lo que en este caso this es equivalente a la persona. Entonces, ¿por qué no escribir persona en su lugar? Como verá en el artículo JavaScript orientado a objetos para principiantes cuando comenzamos a crear constructores, etc., this es muy útil: siempre asegurará que se usen los valores correctos cuando cambie el contexto de un miembro (por ejemplo, dos instancias de objetos persona diferentes) puede tener diferentes nombres, pero querrá usar su propio nombre al decir su saludo).

Vamos a ilustrar lo que queremos decir con un par de objetos persona simplificados:

    var persona1 = {
      nombre: 'Chris',
      saludo: function() {
        alert('¡Hola!, Soy '+ this.nombre + '.');
      }
    }

    var persona2 = {
      nombre: 'Brian',
      saludo: function() {
        alert('¡Hola!, Soy '+ this.nombre + '.');
     }
   }

En este caso, persona1.saludo() mostrará "¡Hola!, Soy Chris"; persona2.saludo() por otro lado mostrará "¡Hola!, Soy Brian", aunque el código del método es exactamente el mismo en cada caso. Como dijimos antes, this es igual al objeto en el que está el código; esto no es muy útil cuando se escriben objetos literales a mano, pero realmente se vuelve útil cuando se generan objetos dinámicamente (por ejemplo, usando constructores) Todo se aclarará más adelante.
Has estado usando objetos todo el tiempo

A medida que has estado repasando estos ejemplos, probablemente hayas pensando que la notación de puntos que has usando es muy familiar. ¡Eso es porque la has estado usando a lo largo del curso! Cada vez que hemos estado trabajando en un ejemplo que utiliza una API de navegador incorporada o un objeto JavaScript, hemos estado usando objetos, porque tales características se crean usando exactamente el mismo tipo de estructuras de objetos que hemos estado viendo aquí, aunque más complejos que nuestros propios ejemplos personalizados.

Entonces cuando usaste métodos de cadenas de texto como:

    myCadena.split(',');

Estabas usando un método disponible en una instancia de la clase String. Cada vez que creas una cadena en tu código, esa cadena se crea automáticamente como una instancia de String, y por lo tanto tiene varios métodos/propiedades comunes disponibles en ella.

Cuando accedió al modelo de objetos del documento (document object model) usando líneas como esta:

    var miDiv = document.createElement('div');
    var miVideo = document.querySelector('video');

Estaba usando métodos disponibles en una instancia de la clase de Document. Para cada página web cargada, se crea una instancia de Document, llamada document, que representa la estructura, el contenido y otras características de la página entera, como su URL. De nuevo, esto significa que tiene varios métodos/propiedades comunes disponibles en él.

Lo mismo puede decirse de prácticamente cualquier otro Objeto/API incorporado que haya estado utilizando: Array, Math, etc.

Tenga en cuenta que los Objetos/API incorporados no siempre crean instancias de objetos automáticamente. Como ejemplo, la API de Notificaciones, que permite que los navegadores modernos activen las notificaciones del sistema, requiere que tu crees una instancia de un nuevo objeto para cada notificación que deseas disparar. Intente ingresar lo siguiente en su consola de JavaScript:

    var miNotificacion = new Notification('¡Hola!');

De nuevo, veremos que son los constructores en un artículo posterior.

Nota: Es útil pensar en la forma en que los objetos se comunican como paso de mensajes — cuando un objeto necesita otro objeto para realizar algún tipo de acción a menudo enviará un mensaje a otro objeto a través de uno de sus métodos, y esperar una respuesta, que conocemos como un valor de retorno


 Constructores e instancias de objetos
==
Algunas personas sostienen que JavaScript no es un verdadero lenguaje orientado a objetos — por ejemplo, su enunciado class es sólo azúcar sintáctico sobre la herencia prototípica existente y no es una class en el sentido tradicional. JavaScript, utiliza funciones especiales llamadas funciones de constructor para definir objetos y sus características. Son útiles porque a menudo te encontrarás con situaciones en las que no sabes cuántos objetos crearás; los constructores proporcionan los medios para crear tantos objetos como necesite de una manera efectiva, adjuntando datos y funciones a ellos según sea necesario.

Cuando se crea una nueva instancia de objeto a partir de una función constructora, su funcionalidad central (tal como se define en su prototipo, que exploraremos en el artículo Prototipos de objetos) no se copia en el nuevo objeto como lenguajes OO "clásicos", sino que la funcionalidad está vinculada a través de una cadena de referencia llamada cadena prototipo. Así que esto no es una verdadera instanciación, estrictamente hablando, JavaScript usa un mecanismo diferente para compartir funcionalidad entre objetos.

Nota: no ser "OOP clásico" no es necesariamente algo malo; Como se mencionó anteriormente, OOP puede ser muy complejo muy rápidamente, y JavaScript tiene algunas formas agradables de aprovechar las características de OO sin tener que profundizar demasiado en ello.

Exploremos la creación de clases a través de constructores y la creación de instancias de objetos a partir de ellas en JavaScript. En primer lugar, nos gustaría que hiciera una nueva copia local del archivo oojs.html que vimos en nuestro primer artículo de Objetos.
Un ejemplo simple

Comencemos por ver como puedes definir una persona con una funcion normal. Agrega esta funcion dentro del elemento script:

    function createNewPerson(name) {
      var obj = {};
      obj.name = name;
      obj.greeting = function() {
        alert('Hi! I\'m ' + this.name + '.');
      };
      return obj;
    }

Ahora puedes crear una nueva persona llamando esta funcion — prueba con las siguientes lineas en la consola Javascript de tu navegador:

    var salva = createNewPerson('Salva');
    salva.name;
    salva.greeting();

Esto funciona bastante bien, pero es un poco largo; si sabemos que queremos crear un objeto, ¿por qué necesitamos crear explícitamente un nuevo objeto vacío y devolverlo? Afortunadamente, JavaScript nos proporciona un práctico acceso directo, en forma de funciones de constructor - ¡hagamos una ahora!
Reemplaza tu función anterior por la siguiente:

    function Person(name) {
      this.name = name;
      this.greeting = function() {
        alert('Hi! I\'m ' + this.name + '.');
      };
    }

La función constructor es la versión de JavaScript de una clase. Notarás que tiene todas las características que esperas en una función, aunque no devuelve nada o crea explícitamente un objeto - básicamente sólo define propiedades y métodos. Verás que la palabra clave this keyword se está usando aquí también — es básicamente decir que cuando se crea una de estas instancias de objeto, la propiedad name del objeto será igual al valor del nombre pasado a la llamada del constructor, y el método greeting() usará también el valor del nombre pasado a la llamada del constructor.

Nota: Un nombre de función de constructor generalmente comienza con una letra mayúscula — esta convención se utiliza para hacer que las funciones de constructor sean más fáciles de reconocer en el código.

Entonces, ¿cómo llamamos a un constructor para crear algunos objetos?

Agregua las siguientes líneas debajo de su código anterior:

    var person1 = new Person('Bob');
    var person2 = new Person('Sarah');

Guarda el código y vuelva a cargarlo en el navegador, e intenta ingresar las siguientes líneas en la consola Javascript :

    person1.name
    person1.greeting()
    person2.name
    person2.greeting()

¡Guay! Ahora veras que tenemos dos nuevos objetos, cada uno de los cuales está almacenado en un espacio de nombres diferente: para acceder a sus propiedades y métodos, debes llamarlos como person1 o person2; están cuidadosamente empaquetados para que no entren en conflicto con otras funciones. Sin embargo, tienen disponible la misma propiedad name y el método greeting(). Ten en cuenta que están utilizando su propio name que se les asignó cuando se crearon; esta es una razón por la cual es muy importante usar this, para que usen sus propios valores, y no algún otro valor.

Veamos nuevamente las llamadas del constructor:

    var person1 = new Person('Bob');
    var person2 = new Person('Sarah');

En cada caso, la  palabra clave new se usa para indicarle al navegador que queremos crear una nueva instancia de objeto, seguida del nombre de la función con sus parámetros requeridos entre paréntesis, y el resultado se almacena en una variable — muy similar a cómo se llama una función estándar. Cada instancia se crea de acuerdo con esta definición:

    function Person(name) {
       this.name = name;
       this.greeting = function() {
       alert('Hi! I\'m ' + this.name + '.');
       };
    }

Una vez creados los nuevos objetos, las variables person1 y person2 contienen los siguientes objetos:

    {
       name: 'Bob',
       greeting: function() {
           alert('Hi! I\'m ' + this.name + '.');
       }
    }

    {
      name: 'Sarah',
      greeting: function() {
           alert('Hi! I\'m ' + this.name + '.');
      }
    }

Tenga en cuenta que cuando llamamos a nuestra función  constructor, estamos definiendo greeting() cada vez, lo cual no es lo ideal. Para evitar esto, podemos definir funciones en el prototipo, que veremos más adelante.

Prototipos de objetos
==

Los prototipos son un mecanismo mediante el cual los objetos en JavaScript heredan características entre sí. En este artículo, explicaremos como funcionan los prototipos y también cómo se pueden usar las propiedades de los prototipos para añadir métodos a los contructores existentes.
Prerequisites: 	Conocer las funciones en Javascript, conocimientos básicos de Javascript (ver Primeros Pasos y Building blocks) y Javascript orientado a Objetos (ver Introducción a Objetos).
Objective: 	Comprender los prototipos de objectos de Javascript, cómo la cadena de prototype funciona, y cómo añadir nuevos métodos a la propiedad prototype.
¿Un lenguaje basado en prototipos?
Sección

JavaScript es a menudo descrito como un lenguaje basado en prototipos. Para proporcionar mecanismos de herencia los objetos pueden tener un prototipo (objeto prototipo) asociado, que actúa como una plantilla desde la que el objeto puede heredar métodos y propiedades. Un objeto prototipo puede tener, a su vez, otro objeto prototipo asociado desde el que heredar métodos y propiedades. Esto es conocido como la cadena de prototipos, y es la razón por la que los objetos pueden tener métodos y propiedades disponibles que no han sido declarados por ellos mismos.

Aunque para ser exactos, los métodos y propiedades son definidas en la propiedad prototype, que reside en la función constructor del objeto, no en la instancia del objeto.

Así pues tenemos que, en JavaScript, se establece un enlace entre la instancia del objeto y su prototipo (este se encuentra en la propiedad __proto__ de la instancia, que es inicializada por la propiedad prototype del constructor). Y, como ya se ha comentado, el objeto tendrá acceso a una serie de métodos y propiedades que se encuentran a lo largo de la cadena de prototipos asociada.

Nota: Es importante entender que, tanto el prototipo de la instancia de un objeto (al cual se accede mediante Object.getPrototypeOf(obj), o a través de la propiedad __proto__) como el prototipo que contiene el constructor (que se encuentra en la propiedad prototype del constructor) hacen referencia al mismo objeto.

Vamos a echar un vistazo a algunos ejemplos para intentar aclarar estos conceptos.
Entendiendo objectos prototipos
Sección

Volvamos al ejemplo anterior en el que acabamos definiendo nuestro constructor Person() — cargue el ejemplo en su navegador. Si aún no lo tienes luego de haber trabajado el último artículo, usa nuestro ejemplo oojs-class-further-exercises.html (vea también el código fuente).

En este ejemplo, hemos definido una función constructor, así:

    function Persona(nombre, apellido, edad, genero, intereses) {
       this.first = first;
       this.last = last;

    }

We have then created an object instance like this:

    var person1 = new Persona('Bob', 'Smith', 32, 'hombre', ['music', 'skiing']);

Si tipea "person1." en su consola JavaScript, debería ver que el navegador intenta completarlo automáticamente con los nombres de miembro disponibles en este objeto:

En esta lista, podra ver los miembros definidos en el objeto prototipo de person1, que es la Persona() (Persona() es el constructor) - nombre, edad, género, intereses, biografía y saludos. Sin embargo, también verá algunos otros miembros - watch, valueOf, etc - que están definidos en el objeto prototipo de Persona() 's, que es un Objeto (Object). Esto demuestra que el prototipo cadena funciona.

Entonces, ¿qué sucede si llama a un método en person1, que está definido en Object? Por ejemplo:

    person1.valueOf()

Este método simplemente retornará el valor del objeto sobre el que se llama - ¡pruébalo y verás! En este caso, lo que pasa es que:

El navegador comprueba inicialmente si el objeto person1 tiene un método valueOf() disponible en él.
Si No lo hace, entonces el navegador comprueba si el objeto prototipo del objeto person1 (el prototipo del constructor de Person()) tiene un método valueOf() disponible en él.
Si tampoco lo hace, entonces el navegador comprueba si el objeto prototipo del objeto prototipo del constructor Persona() (Objeto() prototipo del objeto prototipo del constructor) tiene un método valueOf() disponible en él. Lo hace, así que es llamado, y todo funciona!

ota: Queremos reiterar que los métodos y propiedades no se copian de un objeto a otro en la cadena del prototipo, sino que se accede a ellos subiendo por la cadena como se ha descrito anteriormente.

Nota: No existe oficialmente una forma de acceder directamente al objeto prototipo de un objeto - los "enlaces" entre los elementos de la cadena están definidos en una propiedad interna, denominada [[prototipo]] en la especificación del lenguaje JavaScript (ver ECMAScript). La mayoría de los navegadores modernos, sin embargo, tienen una propiedad disponible llamada __proto__ (es decir, 2 subrayados en cada lado), que contiene el objeto prototipo del constructor del objeto. Por ejemplo, pruebe person1.__proto__ y person1.__proto__.__proto__ para ver cómo se ve la cadena en código!

Desde ECMAScript 2015 se puede acceder indirectamente al objeto prototipo de un objeto mediante Object.getPrototypeOf(obj).
Propiedades del prototipo: Donde se definen los miembros hereditarios
Sección

Entonces, ¿dónde se definen las propiedades y métodos heredados? Si miras la página de referencia del objeto, verás en la parte izquierda un gran número de propiedades y métodos - muchos más que el número de miembros heredados que vimos disponibles en el objeto person1. Algunos son hereditarios y otros no, ¿por qué?

La respuesta es que los heredados son los que están definidos en la propiedad prototipo (podría llamarse subespacio de nombres), es decir, los que empiezan con Object.prototype, y no los que empiezan sólo con Object. El valor de la propiedad del prototipo es un objeto, que es básicamente un repositorio(bucket) para almacenar propiedades y métodos que queremos que sean heredados por los objetos más abajo en la cadena del prototipo.

Así que Object.prototype.watch(), Object.prototype.valueOf(), etc., están disponibles para cualquier tipo de objeto que herede de Object.prototype, incluyendo nuevas instancias de objeto creadas desde el constructor.

Object.is(), Object.keys(), y otros miembros no definidos dentro del prototipo del repositorio(bucket) no son heredados por instancias de objeto o tipos de objeto que heredan de Object.prototype. Sino que son métodos/propiedades disponibles sólo en el propio constructor Object().

Nota: Esto parece extraño - ¿cómo se puede tener un método definido en un constructor, que en sí mismo es una función? Bueno, una función es también un tipo de objeto - vea la referencia del constructor de Function() si no nos cree.

Puede comprobar las propiedades de los prototipos existentes - vuelva a nuestro ejemplo anterior e intente introducir lo siguiente en la consola JavaScript:

    Person.prototype

El resultado no le mostrará mucho - después de todo, no hemos definido nada en el prototipo de nuestro constructor personalizado! Por defecto, el prototipo de un constructor siempre comienza vacío. Ahora intente lo siguiente:

    Object.prototype

Verá un gran número de métodos definidos en la propiedad Prototype de Object, que están disponibles en los objetos que heredan de Object, como se ha mostrado anteriormente.

Verá otros ejemplos de herencia de cadena de prototipos en todo JavaScript - intente buscar los métodos y propiedades definidas en el prototipo de los objetos globales String, Date, Number y Array, por ejemplo. Todos ellos tienen un número de miembros definidos en su prototipo, por lo que, por ejemplo, cuando se crea una cadena, como ésta:

    var myString = 'Esto es mi String.';

myString inmediatamente tiene una serie de métodos útiles disponibles en él, como split(), indexOf(), replace(), etc.

Importante: La propiedad prototipo es una de las partes más confusamente nombradas de JavaScript - podría pensarse que apunta al objeto prototipo del objeto actual, pero no lo hace (es un objeto interno al que puede accederse mediante __proto__, ¿recuerda?). en su lugar, el prototipo es una propiedad que contiene un objeto en el que se definen los miembros en el que se desea que se hereden.
Revisando create()

Anteriormente mostramos cómo Object.create() crea una nueva instancia de objeto.

Por ejemplo, pruebe esto en la consola JavaScript de su ejemplo anterior:

    var person2 = Object.create(person1);

Lo que hace create() es crear un nuevo objeto a partir de un objeto prototipo específico. Aquí, la person2 se crea utilizando la person1 como objeto prototipo. Puede comprobarlo introduciendo lo siguiente en la consola:

    person2.__proto__

Esto devolverá el objeto Persona.
Propiedades del constructor

Cada función de constructor tiene una propiedad de prototipo cuyo valor es un objeto que contiene una propiedad de constructor. Esta propiedad del constructor apunta a la función original del constructor. Como verá en la siguiente sección que las propiedades definidas en la propiedad Person.prototype (o en general en la propiedad prototipo de una función de constructor, que es un objeto, como se mencionó en la sección anterior) están disponibles para todos los objetos de instancia creados utilizando el constructor Person(). Por lo tanto, la propiedad del constructor también está disponible tanto para los objetos persona1 como para los objetos persona2.

    Por ejemplo, pruebe estos comandos en la consola:

    person1.constructor
    person2.constructor

Esto devolverá el constructor de Person(), viendo que contiene la definición original de esas instancias

Un truco inteligente es que you puedes colocar paréntesis en el final del constructor (con los parámetros necesarios) para crear otra instancia de un objeto desde es constructor.

    var person3 = new person1.constructor('Karen', 'Stephenson', 26, 'female', ['playing drums', 'mountain climbing']);


    person3.name.first
    person3.age
    person3.bio()
    

Herencia prototípica
==

Hasta ahora hemos visto algo de herencia en acción — hemos visto cómo funcionan las cadenas de prototipos, y cómo los miembros son heredados subiendo una cadena. Pero principalmente esto ha involucrado funciones integradas del navegador. ¿Cómo creamos un objeto en JavaScript que hereda de otro objeto?

Exploremos cómo hacer esto con un ejemplo concreto.

Primeros pasos

Primero que nada, hazte una copia local de nuestro archivo  oojs-class-inheritance-start.html (míralo corriendo en vivo también). Dentro de aquí encontrarás el mismo ejemplo de constructor de Persona() que hemos estado usando a través del módulo, con una ligera diferencia — hemos definido solo las propiedades dentro del constructor:

function Persona(nombrePila, apellido, edad, genero, intereses) {
  this.nombre = {
    nombrePila,
    apellido
  };
  this.edad = edad;
  this.genero = genero;
  this.intereses = intereses;
};

Todos los métodos están definidos en el prototipo del constructor. Por ejemplo:

Persona.prototype.saludo = function() {
  alert('¡Hola! soy ' + this.nombre.nombrePila + '.');
};

Nota: En el código fuente, también verá los métodos bio() y despedida() definidos. Más tarde verá cómo estos pueden ser heredados por otros constructores.

Digamos que quisieramos crear una clase de Profesor, como la que describimos en nuestra definición inicial orientada a objetos, que hereda todos los miembros de Persona, pero también incluye:

Una nueva propiedad, materia — esto contendrá la materia que el profesor enseña.
Un método actualizado de saludo(), que suena un poco más formal que el método estándar de saludo() — más adecuado para un profesor que se dirige a algunos estudiantes en la escuela.

Definiendo un constructor Profesor()

Lo primero que tenemos que hacer es crear el constructor Profesor()  — añadimos lo siguiente tras el código existente:

    function Profesor(nombrePila, apellido, edad, genero, intereses, materia) {
      Person.call(this, nombrePila, apellido, edad, genero, intereses);
      this.materia = materia;
    }

 

Esto es similar al constructor de Persona en muchos aspectos, pero hay algo extraño aquí que no hemos visto antes: la función call (). Esta función básicamente le permite llamar a una función definida en otro lugar, pero en el contexto actual.
El primer parámetro especifica el valor de this que desea utilizar al ejecutar la función, y los otros parámetros son aquellos que deben pasarse a la función cuando se invoca.

Queremos que el constructor Profesor() tome los mismos parámetros que el constructor Persona() del que está heredando, por lo que los especificamos todos como parámetros en la invocación call().

La última línea dentro del constructor simplemente define la nueva propiedad subject que los profesores tendrán y que las personas genéricas no tienen.

Como nota, podríamos haber simplemente hecho esto:

    function Profesor(nombrePila, apellido, edad, genero, intereses, materia) {
      this.nombre = {
        nombrePila,
        apellido
      };
      this.edad = edad;
      this.genero = genero;
      this.intereses = intereses;
      this.materia = materia;
    }

Pero esto es solo definir las propiedades de nuevo, no heredarlas de Persona(), por lo que anula el punto de lo que estamos tratando de hacer. También lleva más líneas de código.
Heredando de un constructor sin parámetros
Sección

Nótese que si el constructor del cual se está heredando no toma los valores de sus propiedades de parámetros, no se necesita especificarlos como argumentos adicionales en call(). Por ejemplo, si se tuviera algo muy simple como esto:

   function Brick() {
     this.width = 10;
     this.height = 20;
   }

Se podrían heredar las propiedades width y height haciendo esto (así como los otros pasos descritos a continuación, por supuesto):

   function BlueGlassBrick() {
      Brick.call(this);

      this.opacity = 0.5;
      this.color = 'blue';
    }

Nótese que solo especificamos this dentro de call() — no se requieren otros parámetros ya que no estamos heredando ninguna propiedad del padre que sea establecida por parámetros.
Estableciendo el prototipo de Teacher() y su referencia al constructor
Sección

Todo va bien hasta ahora, pero tenemos un problema. Definimos un nuevo constructor, y tiene una propiedad prototype, la cual por defecto solo contiene una referencia a la función constructor en sí misma. No contiene los métodos de la propiedad prototype del constructor Persona. Para ver esto, introduzca Object.getOwnPropertyNames(Profesor.prototype) ya sea en el campo de texto o en la consola de Javascript . Introdúzcalo nuevamente, reemplazando Profesor con Persona. El nuevo constructor tampoco hereda esos métodos. Para ver esto, compare los resultados de Persona.prototype.saludo and Profesor.prototype.saludo. Necesitamos obtener Profesor() para obtener los métodos definidos en el prototipo de Persona(). ¿Cómo lo hacemos?
Añade la siguiente línea debajo de tu adición anterior:

    Teacher.prototype = Object.create(Person.prototype);
    Teacher.prototype.constructor = Teacher;

    Teacher.prototype.greeting = function() {
      var prefix;

      if (this.gender === 'male' || this.gender === 'Male' || this.gender === 'm' || this.gender === 'M') {
        prefix = 'Mr.';
      } else if (this.gender === 'female' || this.gender === 'Female' || this.gender === 'f' || this.gender === 'F') {
        prefix = 'Mrs.';
      } else {
        prefix = 'Mx.';
      }

     alert('Hello. My name is ' + prefix + ' ' + this.name.last + ', and I teach ' + this.subject + '.');
};


    var teacher1 = new Teacher('Dave', 'Griffiths', 31, 'male', ['football', 'cookery'], 'mathematics');

Now save and refresh, and try accessing the properties and methods of your new teacher1 object, for example:

    teacher1.name.first;
    teacher1.interests[0];
    teacher1.bio();
    teacher1.subject;
    teacher1.greeting();
    teacher1.farewell();

