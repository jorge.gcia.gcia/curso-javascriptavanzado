
var Operacion = function (cantidad,nItemCart)
{
  this.cantidad = cantidad;
  this.nItemCart = nItemCart;
  this.interval = [];

  this.restarCantidad = function(cantidad, nItemCart){
    if (cantidad > 0) {
      //var self = this;

      cantidad--;
      this.nItemCart--;

      console.log("estoy restando", cantidad, this.nItemCart); // en esta linea si reconoce la variable
      this.interval.push(setInterval(function (nItemCart) {
        if (cantidad > 0) {
          cantidad--;
          console.log(nItemCart, this.nItemCart); // en esta linea no reconoce la variable
          //console.log(nItemCart, self.nItemCart);
          nItemCart--;
          console.log("estoy restando", cantidad, nItemCart, this.nItemCart); // en esta otra linea tampoco reconoce la variable
          //console.log("estoy restando", cantidad, nItemCart, self.nItemCart);
        }
      }, 150,nItemCart));
    }
  }
}

var thisProblemGo = function () {
    var log = function (message){
        console.log(message);
        var logElement = document.getElementById("results");
        logElement.innerHTML = logElement.innerHTML+message+"<br/>";
    }
    var op = new Operacion (14,14);
    op.restarCantidad(14,14);
}

// desarrollar los parametros out